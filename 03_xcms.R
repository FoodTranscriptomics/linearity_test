library(xcms)



# Peak picking ------------------------------------------------------------
# system.time({
#
#     raw2 <- removePeaks(raw, 1e1)
#     raw2 <- clean(raw2, all=TRUE)

    x_p_all <- findChromPeaks(raw, param = params$CentWave)
# })


plotChromPeakImage(x_p_all, binSize = 10, log=TRUE)

plotChromPeaks(x_p_all, file = 10)


# Split positive and neg mode ---------------------------------------------
x <- list()

x$neg$p <- filterFile(x_p_all,
                      file = pData(x_p_all) %>% {grepl("NEG", .$ion_mode)} %>% which
                      )

x$pos$p <- filterFile(x_p_all,
                      file = pData(x_p_all) %>% {grepl("POS", .$ion_mode)} %>% which
                      )




# Alignment ---------------------------------------------------------------
# Feature grouping
sampleGroups(params$PeakDensity1$neg) <- x$neg$p %>%
                                         pData %>%
                                         {rep("dummy", nrow(.))}

sampleGroups(params$PeakDensity1$pos) <- x$pos$p %>%
                                         pData %>%
                                         {rep("dummy", nrow(.))}

x$neg$pg <- groupChromPeaks(x$neg$p, param = params$PeakDensity1$neg)
x$pos$pg <- groupChromPeaks(x$pos$p, param = params$PeakDensity1$pos)


highlightChromPeaks(x$neg$pg)


# RT aligment
x$neg$pga <- adjustRtime(x$neg$pg, param = params$PeakGroups)
x$pos$pga <- adjustRtime(x$pos$pg, param = params$PeakGroups)


plotAdjustedRtime(x$neg$pga, col = as.factor(sampleGroups(params$PeakDensity1$neg)), peakGroupsCol = "grey", peakGroupsPch = 1)
plotAdjustedRtime(x$pos$pga, col = as.factor(sampleGroups(params$PeakDensity1$pos)), peakGroupsCol = "grey", peakGroupsPch = 1) 


# Feature grouping
sampleGroups(params$PeakDensity2$neg) <- x$neg$pga %>%
                                         pData %>%
                                         {rep("dummy", nrow(.))}

sampleGroups(params$PeakDensity2$pos) <- x$pos$pga %>%
                                         pData %>%
                                         {rep("dummy", nrow(.))}

x$neg$pgag <- groupChromPeaks(x$neg$pga, param = params$PeakDensity2$neg)
x$pos$pgag <- groupChromPeaks(x$pos$pga, param = params$PeakDensity2$pos)




# Gap filling -------------------------------------------------------------
x$neg$pgagf <- fillChromPeaks(x$neg$pgag, params$FillChromPeaks)
x$pos$pgagf <- fillChromPeaks(x$pos$pgag, params$FillChromPeaks)



# Get old style XCMSset ---------------------------------------------------
x$neg$pgagf_xset <- as(x$neg$pgagf, "xcmsSet")
x$pos$pgagf_xset <- as(x$pos$pgagf, "xcmsSet")

